<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
$count = \app\models\Userbot::find()->all();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="shortcut icon" href="web/css/img/logo.png" type="image/x-icon">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Чат-бот отеля "ДЮК"',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [

        ],
    ]);
    NavBar::end();
    ?>
    <?php if(Yii::$app->user->isGuest == false):?>
    <nav id="menuVertical">
        <img id="img_logo_duk" src="web/css/img/logod.jpg" style="
    width: 100px;
    border-radius: 50px;
    height: 100px;
    margin-left: 35px;
">
        <ul id="display_none_pc">
            <li><a href="<?= Url::to(['duk']) ?>">Главная</a></li>
            <li><a href="<?= Url::to(['user']) ?>">Подписчики :<br> <?php echo count($count);?></a></li>
            <li><a href="<?= Url::to(['site/logout']) ?>">Выход</a></li>
        </ul>

        <ul id="display_none_mob">
            <li><a href="<?= Url::to(['duk']) ?>"><i class="fas fa-home fa-2x"></i></a></li>
            <li><a href="<?= Url::to(['user']) ?>"><i class="fas fa-users fa-2x"></i></a></li>
            <li><a href="<?= Url::to(['site/logout']) ?>"><i class="fas fa-door-open fa-2x"></i></i></a></li>
        </ul>



        <?php endif;?>

        <?php
/*        echo Nav::widget([
            'items' => [
                ['label' => 'Изменить данные', 'url' => ['/user/updateform']],
                ['label' => 'Рассылка', 'url' => ['/user/message']],
                [
                    'label' => 'Выход',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
            ],
        ]);
        */?>
    </nav>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<div class="menu"><div class="footerm"> © <?= date('Y') ?><br>
        powered by <a href="https://targetmarket.com.ua/"><font color="#9dacaa">TargetMarket</font></a>
    </div></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
