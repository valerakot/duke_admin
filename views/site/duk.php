<h1>Просмотр переписок</h1>
<?php

/* @var $this yii\web\View */


use yii\helpers\Html;

?>

<form action="?r=site%2Fabout" method="get">
    <div class="form-group">
        <label>От</label>
        <input placeholder="Выберите дату" class="form-control" name="calendar"  class="textbox-n" type="text" value="<?php
        if(!empty($cal)){
            echo $cal;
        }else{
            echo "Выберите дату";
        }
        ?>" onfocus="(this.type='date')"  id="date">
    </div>
    <div class="form-group">
        <label>До</label>
        <input placeholder="Выберите дату" class="form-control" name="calendardo"  class="textbox-n" type="text" value="<?php
        if(!empty($caldo)){
            echo $caldo;
        }else{
            echo $cal;
        }
        ?>" onfocus="(this.type='date')"  id="date">
    </div>
    <div class="form-group">

        <select  class="form-control" id="inputGroupSelect01" name="q">
            <option selected><?php
                if(!empty($q)){
                    echo $q;
                }else{
                    echo "Выберите пользователя";
                }
                ?></option>
            <option>Выберите пользователя</option>
            <?php foreach ($msg as $ms):?>
                <?php if(!empty($ms->user_login) && $ms->user_login != "undefined"):?>
                    <option><?= $ms->user_login?></option>
                <?php endif;?>
            <?php endforeach;?>
        </select></div>
    <div class="form-group">

        <select  class="form-control" id="inputGroupSelect01" name="dolg">
            <option selected><?php
                if(!empty($dolg)){
                    echo $dolg;
                }else{
                    echo "Выберите Администратора";
                }
                ?></option>
            <option>Выберите Администратора</option>
            <?php foreach ($admin as $ad):?>
                <?php if(!empty($ad->admin_key)):?>
                    <option><?= $ad->dolg?></option>
                <?php endif;?>
            <?php endforeach;?>
        </select></div>
    <p><input type="submit" class="btn btn-success" value="Отправить"></p>
</form>
<div id="block_pc">
    <table class="table table-bordered" >
        <thead>
        <tr>
            <th scope="col">Имя</th>
            <th scope="col">Сообщение</th>
            <th scope="col">Время</th>
            <th scope="col">Разница</th>
        </tr>
        </thead>
    <?php if(!empty($q) && !empty($cal) && empty($dolg)):?>
    <?php
        $result=array_merge($query_m,$query_a);

        function sort_date($a_new, $b_new)
        {
            $a_new = strtotime($a_new["date"]);
            $b_new = strtotime($b_new["date"]);
            $sort=$a_new - $b_new;
            return $a_new - $b_new;
        }
        usort($result, "sort_date");?>
    <?php for ($i=0;$i<count($result);$i++) :?>
    <tr>
        <th scope="row">
            <?php
            if($admin_name["name"] == $result[$i]["user"]){
                echo $admin_name["dolg"];
            }else{
                echo  $result[$i]["user"];
            }

            ?>
        </th>
        <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
            <td><?=$result[$i]['date']?></td>
        <td><?php
            $a=$i -1;
            if($a<1){
                $a=0;
            }
            $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
            echo $sort;
            ?></td>
    </tr>
    <?php endfor;?>
    <?php endif;?>
        <?php if(empty($q) && !empty($cal) && empty($dolg)):?>
            <?php
            $result=array_merge($query_m_d,$query_a_d);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style" ><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($q) && empty($cal) && empty($dolg)):?>
            <?php
            $result=array_merge($query_m_n,$query_a_n);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && !empty($cal) && empty($q)):?>
            <?php
            $result=array_merge($query_d_m,$query_d_a);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && !empty($q) && empty($cal)):?>
            <?php
            $result=array_merge($query_d_m,$query_d_a);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && empty($q) && empty($cal)):?>
            <?php
            $result=array_merge($query_m_a,$query_a_a);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>

    </table>
</div>

<div id="block_mob">
    <?php if(!empty($q) && !empty($cal) && empty($dolg)):?>
    <?php
        $result=array_merge($query_m_mob,$query_a_mob);

        function sort_datemob($a_new, $b_new)
        {
            $a_new = strtotime($a_new["date"]);
            $b_new = strtotime($b_new["date"]);
            $sort=$a_new - $b_new;
            return $a_new - $b_new;
        }
        usort($result, "sort_date");?>
    <?php for ($i=0;$i<count($result);$i++) :?>
    <div>
        <div>
            <?php
            if($admin_name["name"] == $result[$i]["user"]){
                echo $admin_name["dolg"];
            }else{
                echo  $result[$i]["user"];
            }

            ?>
        </div>
        <div>
        <?=$result[$i]['message']?>
        </div>
        <div>
            <?=$result[$i]['date']?>
        </div>
        <div>
        <?php
            $a=$i -1;
            if($a<1){
                $a=0;
            }
            $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
            echo $sort;
            ?>
        </div>
    </div>
    <?php endfor;?>
    <?php endif;?>
        <?php if(empty($q) && !empty($cal) && empty($dolg)):?>
            <?php
            $result=array_merge($query_m_d_mob,$query_a_d_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <div style="border-bottom: 1px solid black; margin-bottom: 10px;">
                    <div>
                       <b>Имя:</b> <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </div>
                    <div>
                        <b>Текст:</b><br> <?=$result[$i]['message']?>
                    </div>
                    <div>
                        <b>Дата:</b> <?=$result[$i]['date']?>
                    </div>
                    <div>
                        <b>Разница:</b><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?>
                    </div>
                </div>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($q) && empty($cal) && empty($dolg)):?>
            <?php
            $result=array_merge($query_m_n_mob,$query_a_n_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && !empty($cal) && empty($q)):?>
            <?php
            $result=array_merge($query_d_m_mob,$query_d_a_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && !empty($q) && empty($cal)):?>
            <?php
            $result=array_merge($query_d_m_mob,$query_d_a_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && empty($q) && empty($cal)):?>
            <?php
            $result=array_merge($query_m_a_mob,$query_a_a_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
</div>
