<div id="contionuser">
    <h1>Подписчики: <?php
        if(!empty($model)) {
            echo count($model);
        }else{
            echo "0";
        }?>
    </h1>
    <table class="table table-bordered text-center" style="margin-top: 30px">
        <thead >
        <tr>
            <th style="text-align: center" scope="col">Имя</th>
            <th style="text-align: center" scope="col">Логин</th>
            <th style="text-align: center" scope="col">Чат id</th>
            <th style="text-align: center" scope="col">Дата регистрации</th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($model)):?>

            <?php foreach ($model as $mod):?>
                <tr>
                    <td><?=$mod["user"]?></td>
                    <td><?php
                        if(!empty($mod["user_login"]) && $mod["user_login"]!="undefined"){
                            echo $mod["user_login"];
                        }else{
                            echo "Нет";
                        }
                        ?>
                    </td>
                    <td><?=$mod["msg_id_bot"]?></td>
                    <td><?=$mod["date"]?></td>
                </tr>
            <?php endforeach;?>

        <?php endif;?>
        </tbody>
    </table>
</div>