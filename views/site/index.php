<h1>Авторизация</h1>
<?php
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
        'template' => "<div class=\"col-lg-12\">{label}</div><div class=\"col-lg-3 \">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
    ],
]);?>

<?= $form->field($login_model,'login')->textInput()->label('Логин')?>

<?= $form->field($login_model,'password')->passwordInput()->label('Пароль')?>

<div>
    <button class="btn btn-success" type="submit">Вход</button>
</div>

<?php $form = ActiveForm::end();?>

