<h1>Просмотр переписок</h1>
<?php

/* @var $this yii\web\View */
/*var_dump($admin[0]->name);
var_dump($admin[1]->name);
var_dump($admin[2]->name);
exit();*/
/*$user = array("Выберите Пользователя"=>"Выберите Пользователя");
$adminall = array("Выберите Администратора"=>"Выберите Администратора");
foreach ($msg as $us ) {
    if($us["user_login"] != null && $us["user_login"] !="undefined") {
       $user =array_merge($user,array($us["user_login"] => $us["user_login"]));
    }
}
foreach ($admin as $ad ) {
    $adminall=array_merge($adminall,array($ad["dolg"]=>$ad["dolg"]));
}*/
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
$pattern = "/(\d+)-(\d+)-(\d+)/i";
$replacement = "\$3.\$2.\$1";
if(!empty($q)){

    $user = array($q => $q,"Выберите Пользователя"=>"Выберите Пользователя");
    foreach ($msg as $us ) {
        if($us["user_login"] != null && $us["user_login"] !="undefined") {
            $user =array_merge($user,array($us["user_login"] => $us["user_login"]));
        }
    }
}else{
    $user = array("Выберите Пользователя"=>"Выберите Пользователя");
    foreach ($msg as $us ) {
        if($us["user_login"] != null && $us["user_login"] !="undefined") {
            $user =array_merge($user,array($us["user_login"] => $us["user_login"]));
        }
    }
}
if(!empty($dolg)){
    $adminall = array($dolg => $dolg,"Выберите Администратора"=>"Выберите Администратора");
    foreach ($admin as $ad ) {
        $adminall=array_merge($adminall,array($ad["dolg"]=>$ad["dolg"]));
    }
}else{
    $adminall = array("Выберите Администратора"=>"Выберите Администратора");
    foreach ($admin as $ad ) {
        $adminall=array_merge($adminall,array($ad["dolg"]=>$ad["dolg"]));
    }
}
?>
<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal','enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'template' => "<div class=\"col-lg-12\">{label}</div><div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
    ],
]);?>
<?php if(!empty($q) || !empty($cal) || !empty($dolg)):?>
<?= $form->field($model,'cal')->input('date',['value'=> $cal])->label('От')?>
<?= $form->field($model,'caldo')->input('date',['value'=> "$caldo"])->label('До ')?>
<?= $form->field($model,'user')->dropDownList($user)->label('Пользователи');?>
<?= $form->field($model,'admin')->dropDownList($adminall)->label('Администратор');?>
<?php else:?>
<?= $form->field($model,'cal')->input('date')->label('От')?>
<?= $form->field($model,'caldo')->input('date')->label('До ')?>
<?= $form->field($model,'user')->dropDownList($user)->label('Пользователи');?>
<?= $form->field($model,'admin')->dropDownList($adminall)->label('Администратор');?>
<?php endif;?>
<div>
    <button class="btn btn-success" type="submit">Отправить</button>
</div>
<?php $form = ActiveForm::end();?>
<?php Pjax::end(); ?>

<?php if(!empty($q) || !empty($cal) || !empty($dolg)):?>
<hr style="border-top: 1px solid #000"/>
    <div ><strong>Дата от :</strong> <?php echo preg_replace($pattern, $replacement, $cal);?></div>
    <div ><strong>Дата до :</strong> <?php echo preg_replace($pattern, $replacement, $caldo);?></div>
    <div ><strong>Пользователь :</strong> <?php echo $q;?></div>
    <div ><strong>Администратор :</strong> <?php echo $dolg;?></div>
    <hr style="border-top: 1px solid #000"/>
<?php endif;?>

<div id="block_pc">
    <table class="table table-bordered" >
        <thead>
        <tr>
            <th scope="col">Имя</th>
            <th scope="col">Сообщение</th>
            <th scope="col">Время</th>
            <th scope="col">Разница</th>
        </tr>
        </thead>
    <?php if(!empty($q) && !empty($cal) && empty($dolg)):?>
    <?php
        $result=array_merge($query_m,$query_a);

        function sort_date($a_new, $b_new)
        {
            $a_new = strtotime($a_new["date"]);
            $b_new = strtotime($b_new["date"]);
            $sort=$a_new - $b_new;
            return $a_new - $b_new;
        }
        usort($result, "sort_date");?>
    <?php for ($i=0;$i<count($result);$i++) :?>
    <tr>
        <th scope="row">
            <?php
            if($admin_name["name"] == $result[$i]["user"]){
                echo $admin_name["dolg"];
            }else{
                echo  $result[$i]["user"];
            }

            ?>
        </th>
        <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
            <td><?=$result[$i]['date']?></td>
        <td><?php
            $a=$i -1;
            if($a<1){
                $a=0;
            }
            $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
            echo $sort;
            ?></td>
    </tr>
    <?php endfor;?>
    <?php endif;?>
        <?php if(empty($q) && !empty($cal) && empty($dolg)):?>
            <?php
            $result=array_merge($query_m_d,$query_a_d);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin[0]->name == $result[$i]["user"]){
                            echo $admin[0]->dolg;
                        }
                        if($admin[1]->name == $result[$i]["user"]){
                            echo $admin[1]->dolg;
                        }
                        if($admin[2]->name == $result[$i]["user"]){
                            echo $admin[2]->dolg;
                        }
                        if($admin[0]->name != $result[$i]["user"] && $admin[1]->name != $result[$i]["user"] && $admin[2]->name != $result[$i]["user"]){
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style" ><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($q) && empty($cal) && empty($dolg)):?>
            <?php
            $result=array_merge($query_m_n,$query_a_n);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin[0]->name == $result[$i]["user"]){
                            echo $admin[0]->dolg;
                        }
                        if($admin[1]->name == $result[$i]["user"]){
                            echo $admin[1]->dolg;
                        }
                        if($admin[2]->name == $result[$i]["user"]){
                            echo $admin[2]->dolg;
                        }
                        if($admin[0]->name != $result[$i]["user"] && $admin[1]->name != $result[$i]["user"] && $admin[2]->name != $result[$i]["user"]){
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && !empty($cal) && empty($q)):?>
            <?php
            $result=array_merge($query_d_m,$query_d_a);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin[0]->name == $result[$i]["user"]){
                            echo $admin[0]->dolg;
                        }
                        if($admin[1]->name == $result[$i]["user"]){
                            echo $admin[1]->dolg;
                        }
                        if($admin[2]->name == $result[$i]["user"]){
                            echo $admin[2]->dolg;
                        }
                        if($admin[0]->name != $result[$i]["user"] && $admin[1]->name != $result[$i]["user"] && $admin[2]->name != $result[$i]["user"]){
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && !empty($q) && empty($cal)):?>
            <?php
            $result=array_merge($query_d_m,$query_d_a);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin[0]->name == $result[$i]["user"]){
                            echo $admin[0]->dolg;
                        }
                        if($admin[1]->name == $result[$i]["user"]){
                            echo $admin[1]->dolg;
                        }
                        if($admin[2]->name == $result[$i]["user"]){
                            echo $admin[2]->dolg;
                        }
                        if($admin[0]->name != $result[$i]["user"] && $admin[1]->name != $result[$i]["user"] && $admin[2]->name != $result[$i]["user"]){
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && empty($q) && empty($cal)):?>
            <?php
            $result=array_merge($query_m_a,$query_a_a);

            function sort_date($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin[0]->name == $result[$i]["user"]){
                            echo $admin[0]->dolg;
                        }
                        if($admin[1]->name == $result[$i]["user"]){
                            echo $admin[1]->dolg;
                        }
                        if($admin[2]->name == $result[$i]["user"]){
                            echo $admin[2]->dolg;
                        }
                        if($admin[0]->name != $result[$i]["user"] && $admin[1]->name != $result[$i]["user"] && $admin[2]->name != $result[$i]["user"]){
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>

    </table>
</div>

<div id="block_mob">
    <?php if(!empty($q) && !empty($cal) && empty($dolg)):?>
    <?php
        $result=array_merge($query_m_mob,$query_a_mob);

        function sort_datemob($a_new, $b_new)
        {
            $a_new = strtotime($a_new["date"]);
            $b_new = strtotime($b_new["date"]);
            $sort=$a_new - $b_new;
            return $a_new - $b_new;
        }
        usort($result, "sort_date");?>
    <?php for ($i=0;$i<count($result);$i++) :?>
    <div>
        <div>
            <?php
            if($admin_name["name"] == $result[$i]["user"]){
                echo $admin_name["dolg"];
            }else{
                echo  $result[$i]["user"];
            }

            ?>
        </div>
        <div>
        <?=$result[$i]['message']?>
        </div>
        <div>
            <?=$result[$i]['date']?>
        </div>
        <div>
        <?php
            $a=$i -1;
            if($a<1){
                $a=0;
            }
            $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
            echo $sort;
            ?>
        </div>
    </div>
    <?php endfor;?>
    <?php endif;?>
        <?php if(empty($q) && !empty($cal) && empty($dolg)):?>
            <?php
            $result=array_merge($query_m_d_mob,$query_a_d_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <div style="border-bottom: 1px solid black; margin-bottom: 10px;">
                    <div>
                       <b>Имя:</b> <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </div>
                    <div>
                        <b>Текст:</b><br> <?=$result[$i]['message']?>
                    </div>
                    <div>
                        <b>Дата:</b> <?=$result[$i]['date']?>
                    </div>
                    <div>
                        <b>Разница:</b><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?>
                    </div>
                </div>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($q) && empty($cal) && empty($dolg)):?>
            <?php
            $result=array_merge($query_m_n_mob,$query_a_n_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name_mob["name"] == $result[$i]["user"]){
                            echo $admin_name_mob["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && !empty($cal) && empty($q)):?>
            <?php
            $result=array_merge($query_d_m_mob,$query_d_a_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && !empty($q) && empty($cal)):?>
            <?php
            $result=array_merge($query_d_m_mob,$query_d_a_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
        <?php if(!empty($dolg) && empty($q) && empty($cal)):?>
            <?php
            $result=array_merge($query_m_a_mob,$query_a_a_mob);

            function sort_datemob($a_new, $b_new)
            {
                $a_new = strtotime($a_new["date"]);
                $b_new = strtotime($b_new["date"]);
                return $a_new - $b_new;
            }
            usort($result, "sort_date");?>
            <?php for ($i=0;$i<count($result);$i++) :?>
                <tr>
                    <th scope="row">
                        <?php
                        if($admin_name["name"] == $result[$i]["user"]){
                            echo $admin_name["dolg"];
                        }else{
                            echo  $result[$i]["user"];
                        }

                        ?>
                    </th>
                    <td><div class = "msg_text_style"><?=$result[$i]['message']?></div></td>
                    <td><?=$result[$i]['date']?></td>
                    <td><?php
                        $a=$i -1;
                        if($a<1){
                            $a=0;
                        }
                        $sort=strtotime($result[$i]['date'])-strtotime($result[$a]['date']);
                        echo $sort;
                        ?></td>
                </tr>
            <?php endfor;?>
        <?php endif;?>
</div>
