<?php
namespace app\models;

use yii\base\Model;
class Login extends Model
{
    public $login;
    public $password;
    public function rules()
    {
        return [
            [['login','password'],'required'],
            ['password','validatePassword'] //собственная функция для валидации пароля
        ];
    }
    public function validatePassword($attribute,$params)
    {
        if(!$this->hasErrors()) // если нет ошибок в валидации
        {
            $admin = $this->getAdmin(); // получаем пользователя для дальнейшего сравнения пароля
            if(!$admin || !$admin->validatePassword($this->password))
            {
                //если мы НЕ нашли в базе такого пользователя
                //или введенный пароль и пароль пользователя в базе НЕ равны ТО,
                $this->addError($attribute,'Пароль или логин введены неверно');
                //добавляем новую ошибку для атрибута password о том что пароль или имейл введены не верно
            }
        }
    }
    public function getAdmin()
    {
        return User::findOne(['login'=>$this->login]); // а получаем мы его по введенному имейлу
    }
}