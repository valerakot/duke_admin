<?php
/**
 * Created by PhpStorm.
 * User: Valera
 * Date: 17.09.2018
 * Time: 10:04
 */
namespace app\models;
class Userbot extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }
    public function getAnswer(){
        return $this->hasMany(Adminanswer::className(),['msg_id_bot'=>'msg_chat_id']);
    }
    public function getMessage(){
        return $this->hasMany(Message::className(),['msg_id_bot'=>'msg_user_id']);
    }

}