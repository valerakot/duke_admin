<?php
/**
 * Created by PhpStorm.
 * User: Valera
 * Date: 17.09.2018
 * Time: 10:07
 */
namespace app\models;
class Adminanswer extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'admin_answer';
    }
    public function getUser(){
        return $this->hasOne(User::className(),['msg_chat_id'=>'msg_id_bot']);
    }
}