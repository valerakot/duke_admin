<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
class Adminuser extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'adminuser';
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    //=============================================

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

}
