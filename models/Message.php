<?php
/**
 * Created by PhpStorm.
 * User: Valera
 * Date: 17.09.2018
 * Time: 10:07
 */
namespace app\models;
class Message extends \yii\db\ActiveRecord
{
 public static function tableName()
 {
     return 'message';
 }
    public function getUser(){
        return $this->hasOne(User::className(),['msg_user_id'=>'msg_id_bot']);
    }
}