<?php

namespace app\controllers;

use app\models\Admin;
use app\models\Adminanswer;
use app\models\Fortmsearch;
use app\models\Login;
use app\models\Userbot;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Message;
use app\models\ContactForm;
use app\models\User;

class SiteController extends Controller{


    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest)
        {
            return $this->redirect(['duk/duk']);
        }
        $login_model = new Login();
        if( Yii::$app->request->post('Login'))
        {
            $login_model->attributes = Yii::$app->request->post('Login');
            if($login_model->validate())
            {
                Yii::$app->user->login($login_model->getAdmin());
                return $this->redirect(['duk/duk']);
            }
        }
        return $this->render('index',['login_model'=>$login_model]);
    }
    public function actionLogout()
    {
        if(!Yii::$app->user->isGuest)
        {
            Yii::$app->user->logout();
            return $this->redirect(['index']);
        }
    }
    public function actionUser()
    {
        $model = Userbot::find()->asArray()->all();

        return $this->render('user', ['model' => $model]);
    }

}
