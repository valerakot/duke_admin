<?php
/**
 * Created by PhpStorm.
 * User: Valera
 * Date: 13.11.2018
 * Time: 10:43
 */

namespace app\controllers;

use app\models\Admin;
use app\models\Adminanswer;
use app\models\Fortmsearch;
use app\models\Userbot;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Message;
use app\models\ContactForm;
use app\models\User;

class DukController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout','duk','user'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionDuk()

    {
        $admin_name_mob = null;
        $id = null;
        $admin_name_pc = null;
        $model = null;
        $query_d_a_mob = null;
        $query_d_m_mob = null;
        $query_m_mob = null;
        $query_a_mob = null;
        $query_m_d_mob = null;
        $query_a_d_mob = null;
        $query_a_a_mob = null;
        $query_m_a_mob = null;
        $query_a_n_mob = null;
        $query_m_n_mob = null;
        $q = null;
        $dolg = null;
        $msg = null;
        $admin = null;
        $query_m = null;
        $cal = null;
        $query_a = null;
        $query_m_n = null;
        $query_a_n = null;
        $query_d_m = null;
        $query_m_a = null;
        $query_d_a = null;
        $query_a_a = null;
        $query_m_d = null;
        $query_a_d = null;
        $calendar = null;
        $query_d = null;
        $admin_name = null;
        $caldo = null;
        if (Yii::$app->user->isGuest) {
            $this->redirect(['site/index']);
        } else {
            $model = new Fortmsearch();
            if (isset($_POST['Fortmsearch'])) {
                if (isset($_POST['Fortmsearch']['cal'])) {
                    $cal = $_POST['Fortmsearch']['cal'];
                } else {
                    $cal = "";
                }
                if (isset($_POST['Fortmsearch']['caldo'])) {
                    if ($_POST['Fortmsearch']['caldo'] == "") {
                        $caldo = $cal;
                    } else {
                        $caldo = $_POST['Fortmsearch']['caldo'];
                    }
                }
                if (isset($_POST['Fortmsearch']['user'])) {
                    if ($_POST['Fortmsearch']['user'] != "Выберите Пользователя") {
                        $q = $_POST['Fortmsearch']['user'];
                    } else {
                        $q = "";
                    }
                } else {
                    $q = "";
                }
                if (isset($_POST['Fortmsearch']['admin'])) {
                    if ($_POST['Fortmsearch']['admin'] != "Выберите Администратора") {
                        $dolg = $_POST['Fortmsearch']['admin'];
                    } else {
                        $dolg = "";
                    }
                } else {
                    $dolg = "";
                }

                $query = Userbot::find()->where(['user_login' => $q])->limit(1)->one();
                $dolgnost = Admin::find()->where(['dolg' => $dolg])->limit(1)->one();
                $calendar = Message::find()->where(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();

                if ($query !== null) {
                    $id = $query->msg_id_bot;
                    $query_m_n = Message::find()->asArray()->where(['msg_chat_id' => $id])->all();
                    $query_a_n = Adminanswer::find()->asArray()->where(['msg_user_id' => $id])->all();
                    $query_a_n_pc = Adminanswer::find()->asArray()->where(['msg_user_id' => $id])->one();
                    $query_m_n_mob = Message::find()->asArray()->where(['msg_chat_id' => $id])->all();
                    $query_a_n_mob = Adminanswer::find()->asArray()->where(['msg_user_id' => $id])->all();
                    $query_a_n_mob_mob = Adminanswer::find()->asArray()->where(['msg_user_id' => $id])->one();
                    $admin_name_pc = Admin::find()->asArray()->where(['msg_chat_id' => $query_a_n_pc["msg_admin_id"]])->one();
                    $admin_name_mob = Admin::find()->asArray()->where(['msg_chat_id' => $query_a_n_mob_mob["msg_admin_id"]])->one();
                }
                if ($dolgnost !== null) {
                    $ida = $dolgnost->msg_chat_id;
                    $query_m_a = Message::find()->asArray()->where(['msg_admin_id' => $ida])->all();
                    $query_a_a = Adminanswer::find()->asArray()->where(['msg_admin_id' => $ida])->all();
                    $query_m_a_mob = Message::find()->asArray()->where(['msg_admin_id' => $ida])->all();
                    $query_a_a_mob = Adminanswer::find()->asArray()->where(['msg_admin_id' => $ida])->all();
                    $admin_name = Admin::find()->asArray()->where(['msg_chat_id' => $ida])->one();
                }
                if ($calendar !== null) {
                    $query_m_d = Message::find()->asArray()->where(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_a_d = Adminanswer::find()->asArray()->where(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_m_d_mob = Message::find()->asArray()->where(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_a_d_mob = Adminanswer::find()->asArray()->where(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_a_de = Adminanswer::find()->asArray()->where(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->one();
                    $admin_name = Admin::find()->asArray()->where(['msg_chat_id' => $query_a_de["msg_admin_id"]])->one();
                }
                if ($query !== null && $calendar !== null) {
                    $id = $query->msg_id_bot;
                    $query_m = Message::find()->asArray()->where(['like', 'msg_chat_id', $id])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_a = Adminanswer::find()->asArray()->where(['like', 'msg_user_id', $id])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_m_mob = Message::find()->asArray()->where(['like', 'msg_chat_id', $id])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_a_mob = Adminanswer::find()->asArray()->where(['like', 'msg_user_id', $id])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_am = Adminanswer::find()->asArray()->where(['like', 'msg_user_id', $id])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->one();
                    $admin_name = Admin::find()->asArray()->where(['msg_chat_id' => $query_am["msg_admin_id"]])->one();

                }
                if ($dolgnost !== null && $calendar !== null) {
                    $ida = $dolgnost->msg_chat_id;
                    $query_d_m = Message::find()->asArray()->where(['like', 'msg_admin_id', $ida])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_d_a = Adminanswer::find()->asArray()->where(['like', 'msg_admin_id', $ida])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_d_m_mob = Message::find()->asArray()->where(['like', 'msg_admin_id', $ida])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $query_d_a_mob = Adminanswer::find()->asArray()->where(['like', 'msg_admin_id', $ida])->andWhere(['between', 'date', $cal . " 00:00:00", $caldo . " 23:59:00"])->all();
                    $admin_name = Admin::find()->asArray()->where(['msg_chat_id' => $ida])->one();
                }
                if ($dolgnost !== null && $query !== null) {
                    $ida = $dolgnost->msg_chat_id;
                    $id = $query->msg_id_bot;
                    $query_d_m = Message::find()->asArray()->where(['like', 'msg_admin_id', $ida])->andWhere(['msg_chat_id' => $id])->all();
                    $query_d_a = Adminanswer::find()->asArray()->where(['like', 'msg_admin_id', $ida])->andWhere(['msg_user_id' => $id])->all();
                    $query_d_m_mob = Message::find()->asArray()->where(['like', 'msg_admin_id', $ida])->andWhere(['msg_chat_id' => $id])->all();
                    $query_d_a_mob = Adminanswer::find()->asArray()->where(['like', 'msg_admin_id', $ida])->andWhere(['msg_user_id' => $id])->all();
                    $admin_name = Admin::find()->asArray()->where(['msg_chat_id' => $ida])->one();
                }
            }


            $msg = Userbot::find()->all();
            $admin = Admin::find()->all();
            return $this->render('duk', compact('id','admin_name_mob','admin_name_pc','model', 'query_d_a_mob', 'query_d_m_mob', 'query_m_mob', 'query_a_mob', 'query_m_d_mob', 'query_a_d_mob', 'query_a_a_mob', 'query_m_a_mob', 'query_a_n_mob', 'query_m_n_mob', 'q', 'dolg', 'msg', 'admin', 'query_m', 'query_a', 'cal', 'query_m_n', 'query_a_n', 'query_d_m', 'query_d_a', 'query_m_a', 'query_a_a', 'query_m_d', 'query_a_d', 'calendar', 'query_d', 'admin_name', 'caldo'));
        }
    }



    public function actionUser()
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(['site/index']);
        } else {
            $model = Userbot::find()->asArray()->all();

            return $this->render('user', ['model' => $model]);
        }
    }
}